# Сначала собираем образ для компиляции приложения
FROM golang:1.17 AS build

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o app cmd/main.go

# Образ для запуска приложения
FROM scratch

COPY --from=build /app/app /app

EXPOSE 8080

ENTRYPOINT ["/app"]

