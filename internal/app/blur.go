package app

import (
	"image"
	"image/draw"
	"image/jpeg"
	"os"

	"github.com/disintegration/imaging"
)

func BlurImage(inputPath string, outputPath string) error {
	inputFile, err := os.Open(inputPath)
	if err != nil {
		return err
	}
	defer inputFile.Close()

	img, _, err := image.Decode(inputFile)
	if err != nil {
		return err
	}

	// Применяем размытие
	blurredImg := imaging.Blur(img, 5.0)

	outputFile, err := os.Create(outputPath)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	err = jpeg.Encode(outputFile, blurredImg, nil)
	if err != nil {
		return err
	}

	return nil
}
