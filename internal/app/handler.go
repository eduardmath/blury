package app

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
)

func handleIndex(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Welcome to Image Blur Service"))
}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		file, _, err := r.FormFile("image")
		if err != nil {
			http.Error(w, "Failed to read uploaded file", http.StatusBadRequest)
			return
		}
		defer file.Close()

		// Сохраняем загруженное изображение во временный файл
		tempFile, err := os.CreateTemp("", "upload-*.jpg")
		if err != nil {
			http.Error(w, "Failed to create temporary file", http.StatusInternalServerError)
			return
		}
		defer tempFile.Close()

		_, err = io.Copy(tempFile, file)
		if err != nil {
			http.Error(w, "Failed to save uploaded file", http.StatusInternalServerError)
			return
		}

		// Создаем размытое изображение
		outputPath := filepath.Join("web", "output.jpg")
		err = BlurImage(tempFile.Name(), outputPath)
		if err != nil {
			http.Error(w, "Failed to blur image", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/download", http.StatusSeeOther)
	} else {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func handleDownload(w http.ResponseWriter, r *http.Request) {
	outputPath := filepath.Join("web", "output.jpg")

	file, err := os.Open(outputPath)
	if err != nil {
		http.Error(w, "Failed to open file for download", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	fi, err := file.Stat()
	if err != nil {
		http.Error(w, "Failed to get file info", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", "blurred_image.jpg"))
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", fi.Size()))

	_, err = io.Copy(w, file)
	if err != nil {
		http.Error(w, "Failed to write file to response", http.StatusInternalServerError)
		return
	}
}
